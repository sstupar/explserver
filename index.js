var express = require('express'),
  http = require('http'),
  bodyparser = require('body-parser'),
  io = require('socket.io')(),
  fr = require('./filereader'),
  fs = require('fs'),
  os = require('os'),
  path = require('path'),
  routes = require('./routes');
module.exports = function(opts){
  var conf = {
    serverPort: opts.serverPort || 4550,
    socketPort: opts.socketPort || 5888,
    root: opts.root || '/'
  };
  console.log(conf.root);
  fs.writeFileSync(path.join(__dirname, 'client', 'config.js'),
    'window.explConfig = {socketPort:' + conf.socketPort + '};');
  var app = express();
  //var server = http.createServer(app);
  //server.listen(5896);
  //var io = socketio(server);
  app.engine('html', require('ejs').renderFile);
  app.use('/', express.static(__dirname + '/'));
  app.use(bodyparser.json({ limit: '50mb' }));
  app.use(bodyparser.urlencoded({
    limit: '50mb',
    extended: true
  }));

  var route = routes(app, io);

  io.on('connection', function(socket){
    console.log('connected');
    socket.on('get-files', function(data){
      console.log(data);
      var explPath = path.join(conf.root, data.path);
      var isDir = fs.lstatSync(explPath).isDirectory();

      if(isDir){
        fr.readDir(explPath, function(files){
          io.emit('files', files);
        });
      }else{
        fs.createReadStream(explPath, { encoding: 'utf8' }).on('data', function(doc){
          io.emit('content', doc);
        });
      }
    });

    socket.on('view-log', function(data){
      var explPath = path.join(conf.root, data.path);
      var contentArr = fs.readFileSync(explPath, { encoding: 'utf8' }).split(os.EOL);
      var content = {};
      contentArr.forEach(function(item, i){
        if(!item){
          return;
        }
        content[JSON.parse(item).level + i] = JSON.parse(item);
      });
      io.emit('content', content);
    });
  });
  io.listen(conf.socketPort);
  app.listen(conf.serverPort, function(){
    console.log('server running at http//:127.0.0.1:' + conf.serverPort + '/');
  });
};
