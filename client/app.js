(function () {
    var app = angular.module("logger");
    var socket = io('ws://localhost:' + window.explConfig.socketPort);
    socket.off('connect').on('connect', function () {
        console.log('connected');
        socket.on('disconnect', function () {
        });
        socket.emit('get-files', {
            path: window.explConfig.root || '/'
        });
    });
    app.component('home', {
        templateUrl: './client/home.html',
        controller: home
    });

    function home($scope, $element) {
        var self = this;
        var userPath = {
            currentPath: '/',
            file: '',
            prevPath: []
        };
        socket.on('files', function (data) {
            $scope.$evalAsync(function () {
                self.files = data;
            });
        });
        socket.on('content', function (data) {
            $element.find('#json-renderer').jsonViewer(data);
        });
        function getFiles(path) {
            socket.emit('get-files', {
                path: path || '/'
            });
        }

        function open(file) {
            socket.emit('get-files', {
                path: userPath.currentPath + '/' + file.name
            });
            if(!file.isdir){
                userPath.file = file.name;
                return;
            }
            userPath.prevPath.push(userPath.currentPath);
            userPath.currentPath = userPath.currentPath + '/' + file.name;
            console.log(userPath);
        }

        function back(){
            socket.emit('get-files', {
                path: userPath.prevPath[userPath.prevPath.length -1] || '/'
            });
            userPath.currentPath = userPath.prevPath[userPath.prevPath.length -1] || '/';
            userPath.prevPath.pop();
            console.log(userPath);
        }

        function viewLog(){
            socket.emit('view-log', {
                path: userPath.currentPath + '/' + userPath.file
            });
        }

        self.getFiles = getFiles;
        self.viewLog = viewLog;
        self.open = open;
        self.back = back;
    }


//app.config(function($stateProvider, $urlRouterProvider) {
//    $stateProvider
//        .state('home', {
//            url: '/',
//            template: '<home></home>'
//        });
//    $urlRouterProvider.otherwise('/');
//});
})();