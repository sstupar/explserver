var fs = require('fs');
var path = require('path');

//var html = fs.readFileSync(path.join(__dirname, 'client', 'index.html'));

module.exports = function (app, io) {
    app.get('/', function (req, res) {
        console.log('/');
        res.sendFile('index.html', {
            root: path.join(__dirname, '/client')
        });
    });
};


