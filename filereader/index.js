var fs = require('fs');
var path = require('path');

function readDir(dirpath, cb){
    fs.readdir(dirpath, function(err, data){
        if(err){console.log(err);}
        data = data.map(function(item){
            return {
                name: item,
                isdir: fs.lstatSync(path.join(dirpath, item)).isDirectory()
            }
        });
        cb(data);
    });
}

function readFile(filepath, cb){
    fs.readFile(filepath, function(err, data){
        if(err){console.log(err);}
        cb(data);
    });
}

module.exports = {
    readDir: readDir,
    readFile: readFile
};